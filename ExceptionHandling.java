import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Program Pembagian Bilangan");

        boolean validInput = false;
        do {
            //dalam try terdapat scanner untuk memasukkan input bilangan pembilang dan penyebut dari user.
            //kemudian mendefinisikan int hasil lalu menampilkan hasil pembagiannya.
            //kemudian mengubah validInput menjadi true agar perulangan berhenti
            try{
                System.out.println("Masukkan pembilang: ");
                int pembilang = scanner.nextInt();
                System.out.println("Masukkan penyebut: ");
                int penyebut = scanner.nextInt();

                int hasil = pembagian(pembilang, penyebut);
                System.out.println("Hasil pembagian = "+hasil);
                validInput = true;
            }
            //Terdapat 2 catch
            //catch pertama yaitu untuk menampilkan pesan error jika user menginputkan bilangan yang tidak bulat
            //catch kedua untuk menampilkan pesan error jika user menginputkan bilangan penyebut bernilai nol
            catch (InputMismatchException e){
                System.out.println("Pembilang dan penyebut harus merupakan bilangan bulat");
                scanner.nextLine();
            }
            catch (ArithmeticException e) {
                System.out.println(e.getMessage());
            }
        } while (!validInput);

        scanner.close();
    }

    public static int pembagian(int pembilang, int penyebut) {
        //if untuk mengecek apakah penyebut bernilai nol, jika bernilai nol maka akan throw aritmettiException yang berisi pesan error
        if(penyebut==0){
            throw new ArithmeticException("Penyebut tidak boleh nol!");
        }
        return pembilang / penyebut;
    }
}
